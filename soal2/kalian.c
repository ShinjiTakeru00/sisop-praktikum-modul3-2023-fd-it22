#include <time.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/ipc.h>
#include <sys/shm.h>


#define ROWS 4
#define COLS 5
#define N1 4
#define J1 2
#define N2 2
#define J2 5

int main(void) {

    // Define key for shared memory
    key_t key = 1234;

    // Create shared memory segment
    int shmid = shmget(key, sizeof(int) * ROWS * COLS, IPC_CREAT | 0666);

    // Attach shared memory segment to process address space
    int (*shared_matrix)[COLS] = shmat(shmid, NULL, 0);

    // Generate random values for matrices A and B
    int A[N1][J1], B[N2][J2];
    srand(time(NULL));
    for (int i = 0; i < N1; i++) {
        for (int j = 0; j < J1; j++) {
            A[i][j] = rand() % 5 + 1;
        }
    }
    for (int i = 0; i < N2; i++) {
        for (int j = 0; j < J2; j++) {
            B[i][j] = rand() % 4 + 1;
        }
    }

    // Multiply matrices A and B
    for (int i = 0; i < ROWS; i++) {
        for (int j = 0; j < COLS; j++) {
            shared_matrix[i][j] = 0;
            for (int k = 0; k < J1; k++) {
                shared_matrix[i][j] += A[i][k] * B[k][j];
            }
        }
    }

    // Print the resulting matrix
    printf("Resulting matrix:\n");
    for (int i = 0; i < ROWS; i++) {
        for (int j = 0; j < COLS; j++) {
            printf("%d ", shared_matrix[i][j]);
        }
        printf("\n");
    }

    return 0;
}
