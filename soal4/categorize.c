#include <stdio.h>
#include <stdlib.h>
#include <dirent.h>
#include <string.h>
#include <ctype.h>
#include <time.h>
#include <pthread.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

// Struktur data untuk menyimpan parameter yang diperlukan untuk setiap thread
typedef struct {
    char *src_path;
    char *dst_path;
    char **extensions;
    int ext_count;
    int max;
} thread_data;

// Prototipe fungsi
void *categorize(void *arg);
void log_event(const char *event, const char *data);
int read_extensions(const char *file, char ***extensions);
int read_max(const char *file);
void free_extensions(char **extensions, int n);
char *str_lower(char *str);
int is_target_extension(const char *filename, char **extensions, int n);

int main() {
    char **extensions;
    int ext_count = read_extensions("extensions.txt", &extensions);
    int max = read_max("max.txt");

    pthread_t thread;
    thread_data data;

    data.src_path = "files";
    data.dst_path = "categorized";
    data.extensions = extensions;
    data.ext_count = ext_count;
    data.max = max;

    mkdir(data.dst_path, 0755);
    log_event("MADE", data.dst_path);

    pthread_create(&thread, NULL, categorize, (void *)&data);
    pthread_join(thread, NULL);

    free_extensions(extensions, ext_count);

    return 0;
}

void *categorize(void *arg) {
    thread_data *data = (thread_data *)arg;
    DIR *dir = opendir(data->src_path);
    struct dirent *entry;

    if (!dir) {
        perror("Error opening directory");
        return NULL;
    }

    log_event("ACCESSED", data->src_path);

    while ((entry = readdir(dir)) != NULL) {
        if (entry->d_type == DT_DIR) {
            if (strcmp(entry->d_name, ".") == 0 || strcmp(entry->d_name, "..") == 0)
                continue;

            char sub_src[PATH_MAX], sub_dst[PATH_MAX];
            snprintf(sub_src, PATH_MAX, "%s/%s", data->src_path, entry->d_name);
            snprintf(sub_dst, PATH_MAX, "%s/%s", data->dst_path, entry->d_name);

            thread_data sub_data = *data;
            sub_data.src_path = sub_src;
            sub_data.dst_path = sub_dst;

            pthread_t sub_thread;
            pthread_create(&sub_thread, NULL, categorize, (void *)&sub_data);
            pthread_join(sub_thread, NULL);
        } else {
            if (is_target_extension(entry->d_name, data->extensions, data->ext_count)) {
                char ext[16], src_path[PATH_MAX], dst_path[PATH_MAX];
                sscanf(entry->d_name, "%*[^.].%15s", ext);
                snprintf(src_path, PATH_MAX, "%s/%s", data->src_path, entry->d_name);

                str_lower(ext);
                snprintf(dst_path, PATH_MAX, "%s/%s", data->dst_path, ext);

                struct stat sb;
                if (stat(dst_path, &sb) == -1) {
                    mkdir(dst_path, 0755);
                    log_event("MADE", dst_path);
                }

                snprintf(dst_path, PATH_MAX, "%s/%s/%s", data->dst_path, ext, entry->d_name);

            rename(src_path, dst_path);
            log_event("MOVED", src_path);
            log_event("TO", dst_path);
        } else {
            char src_path[PATH_MAX], dst_path[PATH_MAX];
            snprintf(src_path, PATH_MAX, "%s/%s", data->src_path, entry->d_name);

            snprintf(dst_path, PATH_MAX, "%s/other", data->dst_path);

            struct stat sb;
            if (stat(dst_path, &sb) == -1) {
                mkdir(dst_path, 0755);
                log_event("MADE", dst_path);
            }

            snprintf(dst_path, PATH_MAX, "%s/other/%s", data->dst_path, entry->d_name);

            rename(src_path, dst_path);
            log_event("MOVED", src_path);
            log_event("TO", dst_path);
        }
    }
}

closedir(dir);
return NULL;

}

void log_event(const char *event, const char *data) {
FILE *log = fopen("log.txt", "a");
if (!log) {
perror("Error opening log file");
exit(EXIT_FAILURE);
}

time_t rawtime;
struct tm *timeinfo;
char timestamp[20];

time(&rawtime);
timeinfo = localtime(&rawtime);
strftime(timestamp, 20, "%d-%m-%Y %H:%M:%S", timeinfo);

fprintf(log, "%s %s %s\n", timestamp, event, data);
fclose(log);
}

int read_extensions(const char *file, char ***extensions) {
FILE *f = fopen(file, "r");
if (!f) {
perror("Error opening extensions.txt");
exit(EXIT_FAILURE);
}

int n = 0;
char line[64];

while (fgets(line, sizeof(line), f)) {
    ++n;
}

rewind(f);

*extensions = malloc(n * sizeof(char *));
for (int i = 0; i < n; ++i) {
    (*extensions)[i] = malloc(16 * sizeof(char));
    fscanf(f, "%15s", (*extensions)[i]);
}

fclose(f);
return n;
}

int read_max(const char *file) {
FILE *f = fopen(file, "r");
if (!f) {
perror("Error opening max.txt");
exit(EXIT_FAILURE);
}

int max;
fscanf(f, "%d", &max);
fclose(f);
return max;
}

void free_extensions(char **extensions, int n) {
for (int i = 0; i < n; ++i) {
free(extensions[i]);
}
free(extensions);
}

char *str_lower(char *str) {
for (int i = 0; str[i]; ++i) {
str[i] = tolower((unsigned char)str[i]);
}
return str;
}

int is_target_extension(const char *filename, char **extensions, int n) {
    char ext[16];
    sscanf(filename, "%*[^.].%15s", ext);
    str_lower(ext);

    for (int i = 0; i < n; ++i) {
        if (strcmp(ext, extensions[i]) == 0) {
            return 1;
        }
    }
    return 0;
}

