#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#define MAX_LINE_LENGTH 256

typedef struct {
    char folder[64];
    int accessed_count;
} AccessedFolder;

typedef struct {
    char folder[64];
    int file_count;
} CategorizedFolder;

typedef struct {
    char extension[16];
    int file_count;
} ExtensionCount;

void extract_accessed_folders(FILE *log_file, AccessedFolder *accessed_folders, int *accessed_count) {
    char line[MAX_LINE_LENGTH];
    while (fgets(line, sizeof(line), log_file) != NULL) {
        char timestamp[20], event[10], folder[64];
        sscanf(line, "%s %s %s", timestamp, event, folder);

        if (strcmp(event, "ACCESSED") == 0) {
            int folder_found = 0;
            for (int i = 0; i < *accessed_count; i++) {
                if (strcmp(accessed_folders[i].folder, folder) == 0) {
                    accessed_folders[i].accessed_count++;
                    folder_found = 1;
                    break;
                }
            }
            if (!folder_found) {
                strcpy(accessed_folders[*accessed_count].folder, folder);
                accessed_folders[*accessed_count].accessed_count = 1;
                (*accessed_count)++;
            }
        }
    }
}

void extract_categorized_folders(FILE *log_file, CategorizedFolder *categorized_folders, int *categorized_count) {
    char line[MAX_LINE_LENGTH];
    while (fgets(line, sizeof(line), log_file) != NULL) {
        char timestamp[20], event[10], data[64], folder[64];
        sscanf(line, "%s %s %s %s", timestamp, event, data, folder);

        if (strcmp(event, "MADE") == 0 && strcmp(data, "categorized") == 0) {
            strcpy(categorized_folders[*categorized_count].folder, folder);
            categorized_folders[*categorized_count].file_count = 0;
            (*categorized_count)++;
        }
    }
}

void extract_extension_counts(FILE *log_file, ExtensionCount *extension_counts, int *extension_count) {
    char line[MAX_LINE_LENGTH];
    while (fgets(line, sizeof(line), log_file) != NULL) {
        char timestamp[20], event[10], data[64], folder[64], src_path[256], dst_path[256];
        sscanf(line, "%s %s %s %s %s %*s %*s %s %*s %s", timestamp, event, data, folder, src_path, dst_path);

        if (strcmp(event, "MOVED") == 0 && strcmp(data, "file") == 0) {
            char ext[16];
            sscanf(src_path, "%*[^.].%15s", ext);
            for (int i = 0; ext[i]; ++i) {
                ext[i] = tolower((unsigned char)ext[i]);
            }

            int extension_found = 0;
            for (int i = 0; i < *extension_count; i++) {
                if (strcmp(extension_counts[i].extension, ext) == 0) {
                    extension_counts[i].file_count++;
                    extension_found = 1;
                    break;
                }
            }
            if (!extension_found) {
                strcpy(extension_counts[*extension_count].extension, ext);
                extension_counts[*extension_count].file_count = 1;
                (*extension_count)++;
            }
        }
    }
}


void sort_accessed_folders(AccessedFolder *accessed_folders, int accessed_count) {
for (int i = 0; i < accessed_count - 1; i++) {
for (int j = 0; j < accessed_count - i - 1; j++) {
if (strcmp(accessed_folders[j].folder, accessed_folders[j + 1].folder) > 0) {
AccessedFolder temp = accessed_folders[j];
accessed_folders[j] = accessed_folders[j + 1];
accessed_folders[j + 1] = temp;
}
}
}
}

void sort_categorized_folders(CategorizedFolder *categorized_folders, int categorized_count) {
for (int i = 0; i < categorized_count - 1; i++) {
for (int j = 0; j < categorized_count - i - 1; j++) {
if (strcmp(categorized_folders[j].folder, categorized_folders[j + 1].folder) > 0) {
CategorizedFolder temp = categorized_folders[j];
categorized_folders[j] = categorized_folders[j + 1];
categorized_folders[j + 1] = temp;
}
}
}
}

void sort_extension_counts(ExtensionCount *extension_counts, int extension_count) {
for (int i = 0; i < extension_count - 1; i++) {
for (int j = 0; j < extension_count - i - 1; j++) {
if (strcmp(extension_counts[j].extension, extension_counts[j + 1].extension) > 0) {
ExtensionCount temp = extension_counts[j];
extension_counts[j] = extension_counts[j + 1];
extension_counts[j + 1] = temp;
}
}
}
}

void print_accessed_folders(AccessedFolder *accessed_folders, int accessed_count) {
printf("Accessed Folders:\n");
for (int i = 0; i < accessed_count; i++) {
printf("%s: %d\n", accessed_folders[i].folder, accessed_folders[i].accessed_count);
}
printf("\n");
}

void print_categorized_folders(CategorizedFolder *categorized_folders, int categorized_count) {
printf("Categorized Folders:\n");
for (int i = 0; i < categorized_count; i++) {
printf("%s\n", categorized_folders[i].folder);
}
printf("\n");
}

void print_extension_counts(ExtensionCount *extension_counts, int extension_count) {
printf("Extension Counts:\n");
for (int i = 0; i < extension_count; i++) {
printf("%s: %d\n", extension_counts[i].extension, extension_counts[i].file_count);
}
printf("\n");
}

int main() {
FILE *log_file = fopen("log.txt", "r");
if (!log_file) {
perror("Error opening log file");
return 1;
}
AccessedFolder accessed_folders[100];
int accessed_count = 0;
extract_accessed_folders(log_file, accessed_folders, &accessed_count);
sort_accessed_folders(accessed_folders, accessed_count);
print_accessed_folders(accessed_folders, accessed_count);

rewind(log_file);

CategorizedFolder categorized_folders[100];
int categorized_count = 0;
extract_categorized_folders(log_file, categorized_folders, &categorized_count);
sort_categorized_folders(categorized_folders, categorized_count);
print_categorized_folders(categorized_folders, categorized_count);

rewind(log_file);

ExtensionCount extension_counts[100];
int extension_count = 0;
extract_extension_counts(log_file, extension_counts, &extension_count);
sort_extension_counts(extension_counts, extension_count);
print_extension_counts(extension_counts, extension_count);

fclose(log_file);
return 0;
}
