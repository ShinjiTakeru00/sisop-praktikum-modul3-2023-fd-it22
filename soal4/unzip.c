#include <stdio.h>
#include <stdlib.h>
#include <zip.h>
#include <string.h>
#include <errno.h>
#ifdef _WIN32
#include <direct.h>
#else
#include <sys/stat.h>
#include <sys/types.h> 
#endif

int main(int argc, char **argv) {
    if (argc != 2) {
        fprintf(stderr, "Usage: %s <zipfile>\n", argv[0]);
        return 1;
    }

    int err;
    struct zip *za = zip_open(argv[1], 0, &err);
    if (!za) {
        char buf[1024];
        zip_error_to_str(buf, sizeof(buf), err, errno);
        fprintf(stderr, "Error: %s\n", buf);
        return 1;
    }

    int n = zip_get_num_entries(za, 0);
    for (int i = 0; i < n; ++i) {
        struct zip_stat st;
        zip_stat_index(za, i, 0, &st);

        if (st.name[strlen(st.name) - 1] == '/') {
#ifdef _WIN32
            _mkdir(st.name); 
#else
            mkdir(st.name, 0755);
#endif
        } else {
            struct zip_file *zf = zip_fopen_index(za, i, 0);
            if (!zf) {
                fprintf(stderr, "Error: %s\n", zip_strerror(za));
                return 1;
            }

            FILE *f = fopen(st.name, "wb");
            if (!f) {
                perror("Error");
                return 1;
            }

            char buf[4096];
            int len;
            while ((len = zip_fread(zf, buf, sizeof(buf))) > 0) {
                fwrite(buf, 1, len, f);
            }
            fclose(f);
            zip_fclose(zf);
        }
    }
    zip_close(za);
    return 0;
}
