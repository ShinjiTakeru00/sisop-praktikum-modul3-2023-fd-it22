# sisop-praktikum-modul3-2023-FD-IT22



## Anggota Kelompok IT22

Evan Darya Kusuma (5027211069)

## Nomor 1

**Soal**

Lord Maharaja Baginda El Capitano Harry Maguire, S.Or., S.Kom yang dulunya seorang pemain bola, sekarang sudah pensiun dini karena sering blunder dan merugikan timnya. Ia memutuskan berhenti dan beralih profesi menjadi seorang programmer. Layaknya bola di permainan sepak bola yang perlu dikelola, pada dunia programming pun perlu adanya pengelolaan memori. Maguire berpikir bahwa semakin kecil file akan semakin mudah untuk dikelola dan transfer file-nya juga semakin cepat dan mudah. Dia lantas menemukan Algoritma Huffman untuk proses kompresi lossless. Dengan kepintaran yang pas-pasan dan berbekal modul Sisop, dia berpikir membuat program untuk mengkompres sebuah file. Namun, dia tidak mampu mengerjakannya sendirian, maka bantulah Maguire membuat program tersebut! 
(Wajib menerapkan konsep pipes dan fork seperti yang dijelaskan di modul Sisop. Gunakan 2 pipes dengan diagram seperti di modul 3).
Pada parent process, baca file yang akan dikompresi dan hitung frekuensi kemunculan huruf pada file tersebut. Kirim hasil perhitungan frekuensi tiap huruf ke child process.
Pada child process, lakukan kompresi file dengan menggunakan algoritma Huffman berdasarkan jumlah frekuensi setiap huruf yang telah diterima.
Kemudian (pada child process), simpan Huffman tree pada file terkompresi. Untuk setiap huruf pada file, ubah karakter tersebut menjadi kode Huffman dan kirimkan kode tersebut ke program dekompresi menggunakan pipe.
Kirim hasil kompresi ke parent process. Lalu, di parent process baca Huffman tree dari file terkompresi. Baca kode Huffman dan lakukan dekompresi. 
Di parent process, hitung jumlah bit setelah dilakukan kompresi menggunakan algoritma Huffman dan tampilkan pada layar perbandingan jumlah bit antara setelah dan sebelum dikompres dengan algoritma Huffman.

**Code**
```
#include <ctype.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#define MAX_TREE_HEIGHT 100

typedef struct MinHeapNode {
    char data;
    unsigned freq;
    struct MinHeapNode *left, *right;
} MinHeapNode;

typedef struct MinHeap {
    unsigned size;
    unsigned capacity;
    MinHeapNode** array;
} MinHeap;

MinHeapNode* new_node(char data, unsigned freq);
MinHeap* create_min_heap(unsigned capacity);
void swap_min_heap_nodes(MinHeapNode** a, MinHeapNode** b);
void min_heapify(MinHeap* min_heap, int idx);
int is_size_one(MinHeap* min_heap);
MinHeapNode* extract_min(MinHeap* min_heap);
void insert_min_heap(MinHeap* min_heap, MinHeapNode* min_heap_node);
void build_min_heap(MinHeap* min_heap);
void print_array(int arr[], int n);
int is_leaf(MinHeapNode* root);
MinHeap* create_and_build_min_heap(char data[], int freq[], int size);
MinHeapNode* build_huffman_tree(char data[], int freq[], int size);
void print_codes(MinHeapNode* root, int arr[], int top);
void huffman_codes(char data[], int freq[], int size);
void cek_frek() {
    char alphabet[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    char c;
    char* fileName = "file.txt";
    int* freq = calloc(26, sizeof(int));

    FILE* file = fopen(fileName, "r");
    if (file == NULL) {
        printf("Error: Unable to open the file.\n");
        return;
    }

    while ((c = fgetc(file)) != EOF) {
        int index = (int)toupper(c) - 'A';
        if (index < 0 || index > 25) continue;
        freq[index]++;
    }
    fclose(file);
    huffman_codes(alphabet, freq, strlen(alphabet));
}



int main() {
    cek_frek();

    return 0;
}


MinHeapNode* new_node(char data, unsigned freq) {
    MinHeapNode* temp = (MinHeapNode*) malloc(sizeof(MinHeapNode));
    temp->left = temp->right = NULL;
    temp->data = data;
    temp->freq = freq;
    return temp;
}

MinHeap* create_min_heap(unsigned capacity) {
    MinHeap* min_heap = (MinHeap*) malloc(sizeof(MinHeap));
    min_heap->size = 0;
    min_heap->capacity = capacity;
    min_heap->array = (MinHeapNode**) malloc(min_heap->capacity * sizeof(MinHeapNode*));
    return min_heap;
}

void swap_min_heap_nodes(MinHeapNode** a, MinHeapNode** b) {
    MinHeapNode* temp = *a;
    *a = *b;
    *b = temp;
}

void min_heapify(MinHeap* min_heap, int idx) {
    int smallest = idx;
    int left = 2 * idx + 1;
    int right = 2 * idx + 2;

    if (left < min_heap->size && min_heap->array[left]->freq < min_heap->array[smallest]->freq)
        smallest = left;

    if (right < min_heap->size && min_heap->array[right]->freq < min_heap->array[smallest]->freq)
        smallest = right;

    if (smallest != idx) {
        swap_min_heap_nodes(&min_heap->array[smallest], &min_heap->array[idx]);
        min_heapify(min_heap, smallest);
    }
}

int is_size_one(MinHeap* min_heap) {
    return (min_heap->size == 1);
}

MinHeapNode* extract_min(MinHeap* min_heap) {
    MinHeapNode* temp = min_heap->array[0];
    min_heap->array[0] = min_heap->array[min_heap->size - 1];
    --min_heap->size;
    min_heapify(min_heap, 0);
    return temp;
}

void insert_min_heap(MinHeap* min_heap, MinHeapNode* min_heap_node) {
    ++min_heap->size;
    int i = min_heap->size - 1;
    while (i && min_heap_node->freq < min_heap->array[(i - 1) / 2]->freq) {
    min_heap->array[i] = min_heap->array[(i - 1) / 2];
    i = (i - 1) / 2;
}

min_heap->array[i] = min_heap_node;
}

void build_min_heap(MinHeap* min_heap) {
int n = min_heap->size - 1;
int i;
for (i = (n - 1) / 2; i >= 0; --i) min_heapify(min_heap, i);
}

void print_array(int arr[], int n) {
for (int i = 0; i < n; ++i) printf("%d", arr[i]);
printf("\n");
}

int is_leaf(MinHeapNode* root) {
return !(root->left) && !(root->right);
}

MinHeap* create_and_build_min_heap(char data[], int freq[], int size) {
MinHeap* min_heap = create_min_heap(size);
for (int i = 0; i < size; ++i)
    min_heap->array[i] = new_node(data[i], freq[i]);

min_heap->size = size;
build_min_heap(min_heap);

return min_heap;
}

MinHeapNode* build_huffman_tree(char data[], int freq[], int size) {
    MinHeapNode *left, *right, *top;
    MinHeap* min_heap = create_and_build_min_heap(data, freq, size);

    while (!is_size_one(min_heap)) {
        left = extract_min(min_heap);
        right = extract_min(min_heap);

        top = new_node('$', left->freq + right->freq);
        top->left = left;
        top->right = right;

        insert_min_heap(min_heap, top);
    }

    return extract_min(min_heap);
}


void print_codes(MinHeapNode* root, int arr[], int top) {
if (root->left) {
arr[top] = 0;
print_codes(root->left, arr, top + 1);
}

if (root->right) {
    arr[top] = 1;
    print_codes(root->right, arr, top + 1);
}

if (is_leaf(root)) {
    printf("%c: ", root->data);
    print_array(arr, top);
}
}

void huffman_codes(char data[], int freq[], int size) {
    MinHeapNode* root = build_huffman_tree(data, freq, size);

    int arr[MAX_TREE_HEIGHT], top = 0;

    print_codes(root, arr, top);
}

```

## Nomor 4

**Soal**

Suatu hari, Amin, seorang mahasiswa Informatika mendapati suatu file bernama hehe.zip. Di dalam file .zip tersebut, terdapat sebuah folder bernama files dan file .txt bernama extensions.txt dan max.txt. Setelah melamun beberapa saat, Amin mendapatkan ide untuk merepotkan kalian dengan file tersebut! 
Download dan unzip file tersebut dalam kode c bernama unzip.c.
Selanjutnya, buatlah program categorize.c untuk mengumpulkan (move / copy) file sesuai extension-nya. Extension yang ingin dikumpulkan terdapat dalam file extensions.txt. Buatlah folder categorized dan di dalamnya setiap extension akan dibuatkan folder dengan nama sesuai nama extension-nya dengan nama folder semua lowercase. Akan tetapi, file bisa saja tidak semua lowercase. File lain dengan extension selain yang terdapat dalam .txt files tersebut akan dimasukkan ke folder other.
Pada file max.txt, terdapat angka yang merupakan isi maksimum dari folder tiap extension kecuali folder other. Sehingga, jika penuh, buatlah folder baru dengan format extension (2), extension (3), dan seterusnya.
Output-kan pada terminal banyaknya file tiap extension terurut ascending dengan semua lowercase, beserta other juga dengan format sebagai berikut.
extension_a : banyak_file
extension_b : banyak_file
extension_c : banyak_file
other : banyak_file
Setiap pengaksesan folder, sub-folder, dan semua folder pada program categorize.c wajib menggunakan multithreading. Jika tidak menggunakan akan ada pengurangan nilai.
Dalam setiap pengaksesan folder, pemindahan file, pembuatan folder pada program categorize.c buatlah log dengan format sebagai berikut.
DD-MM-YYYY HH:MM:SS ACCESSED [folder path]
DD-MM-YYYY HH:MM:SS MOVED [extension] file : [src path] > [folder dst]
DD-MM-YYYY HH:MM:SS MADE [folder name]
examples : 
02-05-2023 10:01:02 ACCESSED files
02-05-2023 10:01:03 ACCESSED files/abcd
02-05-2023 10:01:04 MADE categorized
02-05-2023 10:01:05 MADE categorized/jpg
02-05-2023 10:01:06 MOVED jpg file : files/abcd/foto.jpg > categorized/jpg
Catatan:
Path dimulai dari folder files atau categorized
Simpan di dalam log.txt
ACCESSED merupakan folder files beserta dalamnya
Urutan log tidak harus sama
Untuk mengecek apakah log-nya benar, buatlah suatu program baru dengan nama logchecker.c untuk mengekstrak informasi dari log.txt dengan ketentuan sebagai berikut.
Untuk menghitung banyaknya ACCESSED yang dilakukan.
Untuk membuat list seluruh folder yang telah dibuat dan banyaknya file yang dikumpulkan ke folder tersebut, terurut secara ascending.
Untuk menghitung banyaknya total file tiap extension, terurut secara ascending.


**Code**
Unzip.c
```
#include <stdio.h>
#include <stdlib.h>
#include <zip.h>
#include <string.h>
#include <errno.h>
#ifdef _WIN32
#include <direct.h>
#else
#include <sys/stat.h>
#include <sys/types.h> 
#endif

int main(int argc, char **argv) {
    if (argc != 2) {
        fprintf(stderr, "Usage: %s <zipfile>\n", argv[0]);
        return 1;
    }

    int err;
    struct zip *za = zip_open(argv[1], 0, &err);
    if (!za) {
        char buf[1024];
        zip_error_to_str(buf, sizeof(buf), err, errno);
        fprintf(stderr, "Error: %s\n", buf);
        return 1;
    }

    int n = zip_get_num_entries(za, 0);
    for (int i = 0; i < n; ++i) {
        struct zip_stat st;
        zip_stat_index(za, i, 0, &st);

        if (st.name[strlen(st.name) - 1] == '/') {
#ifdef _WIN32
            _mkdir(st.name); 
#else
            mkdir(st.name, 0755);
#endif
        } else {
            struct zip_file *zf = zip_fopen_index(za, i, 0);
            if (!zf) {
                fprintf(stderr, "Error: %s\n", zip_strerror(za));
                return 1;
            }

            FILE *f = fopen(st.name, "wb");
            if (!f) {
                perror("Error");
                return 1;
            }

            char buf[4096];
            int len;
            while ((len = zip_fread(zf, buf, sizeof(buf))) > 0) {
                fwrite(buf, 1, len, f);
            }
            fclose(f);
            zip_fclose(zf);
        }
    }
    zip_close(za);
    return 0;
}
```

Categorize.c
```
#include <stdio.h>
#include <stdlib.h>
#include <dirent.h>
#include <string.h>
#include <ctype.h>
#include <time.h>
#include <pthread.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

// Struktur data untuk menyimpan parameter yang diperlukan untuk setiap thread
typedef struct {
    char *src_path;
    char *dst_path;
    char **extensions;
    int ext_count;
    int max;
} thread_data;

// Prototipe fungsi
void *categorize(void *arg);
void log_event(const char *event, const char *data);
int read_extensions(const char *file, char ***extensions);
int read_max(const char *file);
void free_extensions(char **extensions, int n);
char *str_lower(char *str);
int is_target_extension(const char *filename, char **extensions, int n);

int main() {
    char **extensions;
    int ext_count = read_extensions("extensions.txt", &extensions);
    int max = read_max("max.txt");

    pthread_t thread;
    thread_data data;

    data.src_path = "files";
    data.dst_path = "categorized";
    data.extensions = extensions;
    data.ext_count = ext_count;
    data.max = max;

    mkdir(data.dst_path, 0755);
    log_event("MADE", data.dst_path);

    pthread_create(&thread, NULL, categorize, (void *)&data);
    pthread_join(thread, NULL);

    free_extensions(extensions, ext_count);

    return 0;
}

void *categorize(void *arg) {
    thread_data *data = (thread_data *)arg;
    DIR *dir = opendir(data->src_path);
    struct dirent *entry;

    if (!dir) {
        perror("Error opening directory");
        return NULL;
    }

    log_event("ACCESSED", data->src_path);

    while ((entry = readdir(dir)) != NULL) {
        if (entry->d_type == DT_DIR) {
            if (strcmp(entry->d_name, ".") == 0 || strcmp(entry->d_name, "..") == 0)
                continue;

            char sub_src[PATH_MAX], sub_dst[PATH_MAX];
            snprintf(sub_src, PATH_MAX, "%s/%s", data->src_path, entry->d_name);
            snprintf(sub_dst, PATH_MAX, "%s/%s", data->dst_path, entry->d_name);

            thread_data sub_data = *data;
            sub_data.src_path = sub_src;
            sub_data.dst_path = sub_dst;

            pthread_t sub_thread;
            pthread_create(&sub_thread, NULL, categorize, (void *)&sub_data);
            pthread_join(sub_thread, NULL);
        } else {
            if (is_target_extension(entry->d_name, data->extensions, data->ext_count)) {
                char ext[16], src_path[PATH_MAX], dst_path[PATH_MAX];
                sscanf(entry->d_name, "%*[^.].%15s", ext);
                snprintf(src_path, PATH_MAX, "%s/%s", data->src_path, entry->d_name);

                str_lower(ext);
                snprintf(dst_path, PATH_MAX, "%s/%s", data->dst_path, ext);

                struct stat sb;
                if (stat(dst_path, &sb) == -1) {
                    mkdir(dst_path, 0755);
                    log_event("MADE", dst_path);
                }

                snprintf(dst_path, PATH_MAX, "%s/%s/%s", data->dst_path, ext, entry->d_name);

            rename(src_path, dst_path);
            log_event("MOVED", src_path);
            log_event("TO", dst_path);
        } else {
            char src_path[PATH_MAX], dst_path[PATH_MAX];
            snprintf(src_path, PATH_MAX, "%s/%s", data->src_path, entry->d_name);

            snprintf(dst_path, PATH_MAX, "%s/other", data->dst_path);

            struct stat sb;
            if (stat(dst_path, &sb) == -1) {
                mkdir(dst_path, 0755);
                log_event("MADE", dst_path);
            }

            snprintf(dst_path, PATH_MAX, "%s/other/%s", data->dst_path, entry->d_name);

            rename(src_path, dst_path);
            log_event("MOVED", src_path);
            log_event("TO", dst_path);
        }
    }
}

closedir(dir);
return NULL;

}

void log_event(const char *event, const char *data) {
FILE *log = fopen("log.txt", "a");
if (!log) {
perror("Error opening log file");
exit(EXIT_FAILURE);
}

time_t rawtime;
struct tm *timeinfo;
char timestamp[20];

time(&rawtime);
timeinfo = localtime(&rawtime);
strftime(timestamp, 20, "%d-%m-%Y %H:%M:%S", timeinfo);

fprintf(log, "%s %s %s\n", timestamp, event, data);
fclose(log);
}

int read_extensions(const char *file, char ***extensions) {
FILE *f = fopen(file, "r");
if (!f) {
perror("Error opening extensions.txt");
exit(EXIT_FAILURE);
}

int n = 0;
char line[64];

while (fgets(line, sizeof(line), f)) {
    ++n;
}

rewind(f);

*extensions = malloc(n * sizeof(char *));
for (int i = 0; i < n; ++i) {
    (*extensions)[i] = malloc(16 * sizeof(char));
    fscanf(f, "%15s", (*extensions)[i]);
}

fclose(f);
return n;
}

int read_max(const char *file) {
FILE *f = fopen(file, "r");
if (!f) {
perror("Error opening max.txt");
exit(EXIT_FAILURE);
}

int max;
fscanf(f, "%d", &max);
fclose(f);
return max;
}

void free_extensions(char **extensions, int n) {
for (int i = 0; i < n; ++i) {
free(extensions[i]);
}
free(extensions);
}

char *str_lower(char *str) {
for (int i = 0; str[i]; ++i) {
str[i] = tolower((unsigned char)str[i]);
}
return str;
}

int is_target_extension(const char *filename, char **extensions, int n) {
    char ext[16];
    sscanf(filename, "%*[^.].%15s", ext);
    str_lower(ext);

    for (int i = 0; i < n; ++i) {
        if (strcmp(ext, extensions[i]) == 0) {
            return 1;
        }
    }
    return 0;
}
```
logchecker.c
```
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#define MAX_LINE_LENGTH 256

typedef struct {
    char folder[64];
    int accessed_count;
} AccessedFolder;

typedef struct {
    char folder[64];
    int file_count;
} CategorizedFolder;

typedef struct {
    char extension[16];
    int file_count;
} ExtensionCount;

void extract_accessed_folders(FILE *log_file, AccessedFolder *accessed_folders, int *accessed_count) {
    char line[MAX_LINE_LENGTH];
    while (fgets(line, sizeof(line), log_file) != NULL) {
        char timestamp[20], event[10], folder[64];
        sscanf(line, "%s %s %s", timestamp, event, folder);

        if (strcmp(event, "ACCESSED") == 0) {
            int folder_found = 0;
            for (int i = 0; i < *accessed_count; i++) {
                if (strcmp(accessed_folders[i].folder, folder) == 0) {
                    accessed_folders[i].accessed_count++;
                    folder_found = 1;
                    break;
                }
            }
            if (!folder_found) {
                strcpy(accessed_folders[*accessed_count].folder, folder);
                accessed_folders[*accessed_count].accessed_count = 1;
                (*accessed_count)++;
            }
        }
    }
}

void extract_categorized_folders(FILE *log_file, CategorizedFolder *categorized_folders, int *categorized_count) {
    char line[MAX_LINE_LENGTH];
    while (fgets(line, sizeof(line), log_file) != NULL) {
        char timestamp[20], event[10], data[64], folder[64];
        sscanf(line, "%s %s %s %s", timestamp, event, data, folder);

        if (strcmp(event, "MADE") == 0 && strcmp(data, "categorized") == 0) {
            strcpy(categorized_folders[*categorized_count].folder, folder);
            categorized_folders[*categorized_count].file_count = 0;
            (*categorized_count)++;
        }
    }
}

void extract_extension_counts(FILE *log_file, ExtensionCount *extension_counts, int *extension_count) {
    char line[MAX_LINE_LENGTH];
    while (fgets(line, sizeof(line), log_file) != NULL) {
        char timestamp[20], event[10], data[64], folder[64], src_path[256], dst_path[256];
        sscanf(line, "%s %s %s %s %s %*s %*s %s %*s %s", timestamp, event, data, folder, src_path, dst_path);

        if (strcmp(event, "MOVED") == 0 && strcmp(data, "file") == 0) {
            char ext[16];
            sscanf(src_path, "%*[^.].%15s", ext);
            for (int i = 0; ext[i]; ++i) {
                ext[i] = tolower((unsigned char)ext[i]);
            }

            int extension_found = 0;
            for (int i = 0; i < *extension_count; i++) {
                if (strcmp(extension_counts[i].extension, ext) == 0) {
                    extension_counts[i].file_count++;
                    extension_found = 1;
                    break;
                }
            }
            if (!extension_found) {
                strcpy(extension_counts[*extension_count].extension, ext);
                extension_counts[*extension_count].file_count = 1;
                (*extension_count)++;
            }
        }
    }
}


void sort_accessed_folders(AccessedFolder *accessed_folders, int accessed_count) {
for (int i = 0; i < accessed_count - 1; i++) {
for (int j = 0; j < accessed_count - i - 1; j++) {
if (strcmp(accessed_folders[j].folder, accessed_folders[j + 1].folder) > 0) {
AccessedFolder temp = accessed_folders[j];
accessed_folders[j] = accessed_folders[j + 1];
accessed_folders[j + 1] = temp;
}
}
}
}

void sort_categorized_folders(CategorizedFolder *categorized_folders, int categorized_count) {
for (int i = 0; i < categorized_count - 1; i++) {
for (int j = 0; j < categorized_count - i - 1; j++) {
if (strcmp(categorized_folders[j].folder, categorized_folders[j + 1].folder) > 0) {
CategorizedFolder temp = categorized_folders[j];
categorized_folders[j] = categorized_folders[j + 1];
categorized_folders[j + 1] = temp;
}
}
}
}

void sort_extension_counts(ExtensionCount *extension_counts, int extension_count) {
for (int i = 0; i < extension_count - 1; i++) {
for (int j = 0; j < extension_count - i - 1; j++) {
if (strcmp(extension_counts[j].extension, extension_counts[j + 1].extension) > 0) {
ExtensionCount temp = extension_counts[j];
extension_counts[j] = extension_counts[j + 1];
extension_counts[j + 1] = temp;
}
}
}
}

void print_accessed_folders(AccessedFolder *accessed_folders, int accessed_count) {
printf("Accessed Folders:\n");
for (int i = 0; i < accessed_count; i++) {
printf("%s: %d\n", accessed_folders[i].folder, accessed_folders[i].accessed_count);
}
printf("\n");
}

void print_categorized_folders(CategorizedFolder *categorized_folders, int categorized_count) {
printf("Categorized Folders:\n");
for (int i = 0; i < categorized_count; i++) {
printf("%s\n", categorized_folders[i].folder);
}
printf("\n");
}

void print_extension_counts(ExtensionCount *extension_counts, int extension_count) {
printf("Extension Counts:\n");
for (int i = 0; i < extension_count; i++) {
printf("%s: %d\n", extension_counts[i].extension, extension_counts[i].file_count);
}
printf("\n");
}

int main() {
FILE *log_file = fopen("log.txt", "r");
if (!log_file) {
perror("Error opening log file");
return 1;
}
AccessedFolder accessed_folders[100];
int accessed_count = 0;
extract_accessed_folders(log_file, accessed_folders, &accessed_count);
sort_accessed_folders(accessed_folders, accessed_count);
print_accessed_folders(accessed_folders, accessed_count);

rewind(log_file);

CategorizedFolder categorized_folders[100];
int categorized_count = 0;
extract_categorized_folders(log_file, categorized_folders, &categorized_count);
sort_categorized_folders(categorized_folders, categorized_count);
print_categorized_folders(categorized_folders, categorized_count);

rewind(log_file);

ExtensionCount extension_counts[100];
int extension_count = 0;
extract_extension_counts(log_file, extension_counts, &extension_count);
sort_extension_counts(extension_counts, extension_count);
print_extension_counts(extension_counts, extension_count);

fclose(log_file);
return 0;
}


```
