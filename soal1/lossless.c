#include <ctype.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#define MAX_TREE_HEIGHT 100

typedef struct MinHeapNode {
    char data;
    unsigned freq;
    struct MinHeapNode *left, *right;
} MinHeapNode;

typedef struct MinHeap {
    unsigned size;
    unsigned capacity;
    MinHeapNode** array;
} MinHeap;

MinHeapNode* new_node(char data, unsigned freq);
MinHeap* create_min_heap(unsigned capacity);
void swap_min_heap_nodes(MinHeapNode** a, MinHeapNode** b);
void min_heapify(MinHeap* min_heap, int idx);
int is_size_one(MinHeap* min_heap);
MinHeapNode* extract_min(MinHeap* min_heap);
void insert_min_heap(MinHeap* min_heap, MinHeapNode* min_heap_node);
void build_min_heap(MinHeap* min_heap);
void print_array(int arr[], int n);
int is_leaf(MinHeapNode* root);
MinHeap* create_and_build_min_heap(char data[], int freq[], int size);
MinHeapNode* build_huffman_tree(char data[], int freq[], int size);
void print_codes(MinHeapNode* root, int arr[], int top);
void huffman_codes(char data[], int freq[], int size);
void cek_frek() {
    char alphabet[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    char c;
    char* fileName = "file.txt";
    int* freq = calloc(26, sizeof(int));

    FILE* file = fopen(fileName, "r");
    if (file == NULL) {
        printf("Error: Unable to open the file.\n");
        return;
    }

    while ((c = fgetc(file)) != EOF) {
        int index = (int)toupper(c) - 'A';
        if (index < 0 || index > 25) continue;
        freq[index]++;
    }
    fclose(file);
    huffman_codes(alphabet, freq, strlen(alphabet));
}



int main() {
    cek_frek();

    return 0;
}


MinHeapNode* new_node(char data, unsigned freq) {
    MinHeapNode* temp = (MinHeapNode*) malloc(sizeof(MinHeapNode));
    temp->left = temp->right = NULL;
    temp->data = data;
    temp->freq = freq;
    return temp;
}

MinHeap* create_min_heap(unsigned capacity) {
    MinHeap* min_heap = (MinHeap*) malloc(sizeof(MinHeap));
    min_heap->size = 0;
    min_heap->capacity = capacity;
    min_heap->array = (MinHeapNode**) malloc(min_heap->capacity * sizeof(MinHeapNode*));
    return min_heap;
}

void swap_min_heap_nodes(MinHeapNode** a, MinHeapNode** b) {
    MinHeapNode* temp = *a;
    *a = *b;
    *b = temp;
}

void min_heapify(MinHeap* min_heap, int idx) {
    int smallest = idx;
    int left = 2 * idx + 1;
    int right = 2 * idx + 2;

    if (left < min_heap->size && min_heap->array[left]->freq < min_heap->array[smallest]->freq)
        smallest = left;

    if (right < min_heap->size && min_heap->array[right]->freq < min_heap->array[smallest]->freq)
        smallest = right;

    if (smallest != idx) {
        swap_min_heap_nodes(&min_heap->array[smallest], &min_heap->array[idx]);
        min_heapify(min_heap, smallest);
    }
}

int is_size_one(MinHeap* min_heap) {
    return (min_heap->size == 1);
}

MinHeapNode* extract_min(MinHeap* min_heap) {
    MinHeapNode* temp = min_heap->array[0];
    min_heap->array[0] = min_heap->array[min_heap->size - 1];
    --min_heap->size;
    min_heapify(min_heap, 0);
    return temp;
}

void insert_min_heap(MinHeap* min_heap, MinHeapNode* min_heap_node) {
    ++min_heap->size;
    int i = min_heap->size - 1;
    while (i && min_heap_node->freq < min_heap->array[(i - 1) / 2]->freq) {
    min_heap->array[i] = min_heap->array[(i - 1) / 2];
    i = (i - 1) / 2;
}

min_heap->array[i] = min_heap_node;
}

void build_min_heap(MinHeap* min_heap) {
int n = min_heap->size - 1;
int i;
for (i = (n - 1) / 2; i >= 0; --i) min_heapify(min_heap, i);
}

void print_array(int arr[], int n) {
for (int i = 0; i < n; ++i) printf("%d", arr[i]);
printf("\n");
}

int is_leaf(MinHeapNode* root) {
return !(root->left) && !(root->right);
}

MinHeap* create_and_build_min_heap(char data[], int freq[], int size) {
MinHeap* min_heap = create_min_heap(size);
for (int i = 0; i < size; ++i)
    min_heap->array[i] = new_node(data[i], freq[i]);

min_heap->size = size;
build_min_heap(min_heap);

return min_heap;
}

MinHeapNode* build_huffman_tree(char data[], int freq[], int size) {
    MinHeapNode *left, *right, *top;
    MinHeap* min_heap = create_and_build_min_heap(data, freq, size);

    while (!is_size_one(min_heap)) {
        left = extract_min(min_heap);
        right = extract_min(min_heap);

        top = new_node('$', left->freq + right->freq);
        top->left = left;
        top->right = right;

        insert_min_heap(min_heap, top);
    }

    return extract_min(min_heap);
}


void print_codes(MinHeapNode* root, int arr[], int top) {
if (root->left) {
arr[top] = 0;
print_codes(root->left, arr, top + 1);
}

if (root->right) {
    arr[top] = 1;
    print_codes(root->right, arr, top + 1);
}

if (is_leaf(root)) {
    printf("%c: ", root->data);
    print_array(arr, top);
}
}

void huffman_codes(char data[], int freq[], int size) {
    MinHeapNode* root = build_huffman_tree(data, freq, size);

    int arr[MAX_TREE_HEIGHT], top = 0;

    print_codes(root, arr, top);
}
